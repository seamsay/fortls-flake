{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages.default = let
        pname = "fortls";
        version = "2.13.0";
      in
        pkgs.python3Packages.buildPythonApplication {
          inherit pname version;

          src = pkgs.python3Packages.fetchPypi {
            inherit pname version;
            hash = "sha256-I8UBPo3Y4dZb8HvmENCCe8SKpzMaenzhNhLUxkbQ2zE=";
          };

          propagatedBuildInputs = with pkgs.python3Packages; [
            json5
            packaging
          ];

          meta = {
            homepage = "https://fortls.fortran-lang.org/";
            description = "Language Server Protocol implementation for Fortran.";
            license = pkgs.lib.licenses.mit;
          };
        };

      formatter = pkgs.alejandra;
    });
}
